<?php
require "bootstrap.php";
use Chatter\Models\Message;
$app = new \Slim\App();
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{number}', function($request, $response,$args){
   return $response->write('Hello customer '.$args['number']);
});
$app->get('/customers/{name}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Hello customer '.$args['name'].' your product is ' .$args['pnumber']);
});

$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages = $_message->all();
   $payload = [];
   foreach($messages as $msg){
    $payload[$msg->id] = [
        'body'=>$msg->body,
        'user_id'=>$msg->user_id,
        'created_at'=>$msg->created_at
    ];
   };
   return $response->withStatus(200)->withJson($payload);
});



$app->post('/messages', function($request, $response,$args){
   
   $message = $request->getParsedBodyParam('message','');
   $userid = $request->getParsedBodyParam('userId','');
   $_message = new Message();
   $_message->body = $message;
   $_message->user_id=$userId;
   $_message->save();

   if($_message->id)
   {
       $payload=['message_id'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
   }
   else{
    return $response->withStatus(400);
   }
});

$app->delete('/messages/{message_id}', function($request, $response,$args){
   
$message = Message::find($args['message_id']);
$message->delete();
if($message->exists)
{
    return $response->withStatus(400);
}
else{
    return $response->withStatus(200);
    }
  
});
$app->get('/users', function($request, $response,$args){
    $_user = new User(); 
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->post('/users', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user',''); 
    $usmail = $request->getParsedBodyParam('usmail',''); 
    $_user = new user(); 
     if($user->exists){ 
          return $response->withStatus(400); 
    } 
    else 
    {
        $_user->username = $user;
        $_user->email = $usmail;
        $_user->save();
        if($_user->id){
             $payload = ['user_id'=>$_user->id];
             return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400);
    }
}
});

$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']); 
    $user->delete(); 
    if($user->exists){ 
        return $response->withStatus(400); 
    } else {
        return $response->withStatus(200); 
    }
});






$app->run();